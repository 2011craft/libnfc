package org.craft11;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;

public class API {
    public static void DamageTool(final Player p, final int damage) {
        if (p.getItemInHand().getTypeId() == 0)
            return;

        ItemStack item = p.getItemInHand();
        Tool t = Tool.forId(item.getTypeId());

        if (t == null)
            return;

        int durability = item.getDurability() + damage;
        item.setDurability((short) (durability + damage));

        if (durability >= t.durability) {
            ItemStack[] inventory = p.getInventory().getContents();
            for (int i = 0; i < inventory.length; i++) {
                ItemStack is = inventory[i];
                if (is == null) continue;
                if (is.getTypeId() == item.getTypeId() && is.getDurability() == item.getDurability()) {
                    is.setTypeId(0);
                    is.setAmount(0);
                    break;
                }
            }
            p.getInventory().setContents(inventory);
        }
    }

    public static Integer GetTier(final Player player) {
        final int i = player.getItemInHand().getTypeId();
        final Tool t = Tool.forId(i);
        if (t != null)
            return t.tier;
        return 0;
    }

    public static void DropItem(Location loc, int id, int amount) {
        DropItem(loc, new ItemStack(Material.getMaterial(id), amount));
    }

    public static void DropItem(Location loc, ItemStack is) {
        loc.getWorld().dropItemNaturally(loc, is);
    }

    public static boolean IsSword(ItemStack i) {
        if (i.getTypeId() == 0)
            return false;
        Tool t = Tool.forId(i.getTypeId());
        return t != null && t.getSword() == i.getTypeId();
    }
    
    public static boolean IsHoe(ItemStack i) {
        if (i.getTypeId() == 0)
            return false;
        Tool t = Tool.forId(i.getTypeId());
        return t != null && t.getHoe() == i.getTypeId();
    }

    public static boolean IsPickaxe(ItemStack i) {
        if (i.getTypeId() == 0)
            return false;
        Tool t = Tool.forId(i.getTypeId());
        return t != null && t.getPickaxe() == i.getTypeId();
    }

    public static boolean IsAxe(ItemStack i) {
        if (i.getTypeId() == 0)
            return false;
        Tool t = Tool.forId(i.getTypeId());
        return t != null && t.getAxe() == i.getTypeId();
    }


    public static boolean IsShovel(ItemStack i) {
        if (i.getTypeId() == 0)
            return false;
        Tool t = Tool.forId(i.getTypeId());
        return t != null && t.getShovel() == i.getTypeId();
    }

    public enum Tool {
        //Sword, Shovel, Pickaxe, Axe, Hoe
        WOOD(new int[] {268, 269, 270, 271, 290}, 0, 30),
        STONE(new int[] {272, 273, 274, 275, 291}, 0, 40),
        ALUMINUM(new int[] {522, 502, 479, 562, 542}, 1, 35),
        COPPER(new int[] {523, 503, 481, 563, 543}, 1, 50),
        TIN(new int[] {524, 504, 482, 564, 544}, 1, 40),
        BISMUTH(new int[] {525, 505, 483, 565, 545}, 1, 65),
        ZINC(new int[] {525, 506, 484, 566, 546}, 1, 80),
        LEAD(new int[] {531, 511, 490, 571, 551}, 1, 115),
        NICKEL(new int[] {527, 507, 485, 567, 547}, 2, 85),
        PLATINUM(new int[] {538, 518, 498, 578, 558}, 2, 215),
        BORON(new int[] {539, 519, 499, 579, 559}, 2, 50),
        BRASS(new int[] {540, 520, 500, 580, 560}, 2, 180),
        BRONZE(new int[] {541, 521, 501, 581, 561}, 2, 125),
        SILVER(new int[] {530, 510, 489, 570, 550}, 2, 260),
        GOLD(new int[] {283, 284, 285, 286, 294}, 2, 20),
        IRON(new int[] {257, 258, 256, 267, 292}, 3, 300),
        COBALT(new int[] {528, 508, 486, 568, 548}, 3, 700),
        SILICON(new int[] {532, 512, 491, 572, 552}, 3, 150),
        CHROME(new int[] {533, 513, 492, 573, 553}, 3, 200),
        TUNGSTEN(new int[] {529, 509, 487, 569, 549}, 4, 1750),
        TITANIUM(new int[] {534, 514, 493, 574, 554}, 4, 700),
        STEEL(new int[] {677, 582, 494, 584, 583}, 4, 700),
        RUBY(new int[] {535, 515, 495, 575, 555}, 4, 1450),
        SAPPHIRE(new int[] {536, 516, 496, 576, 556}, 4, 1450),
        ONYX(new int[] {710, 709, 707, 711, 708}, 4, 1450),
        EMERALD(new int[] {537, 517, 497, 577, 557}, 4, 1450),
        MAGNETITE(new int[] {726, 725, 723, 727, 724}, 4, 512),
        DIAMOND(new int[] {276, 277, 278, 279, 293}, 5, 3250),
        OSMIUM(new int[] {695, 694, 691, 696, 693}, 5, 10000)
        ;
        final int[] ids;
        final int tier;
        final int durability;
        final static HashMap<Integer, Tool> idMap = new HashMap<>();
        Tool(int[] ids, int tier, int durability) {
            this.ids = ids;
            this.tier = tier;
            this.durability = durability;
        }

        static {
            for (Tool t : values()) {
                for (int id : t.ids) {
                    idMap.put(id, t);
                }
            }
        }

        public static Tool forId(int id) {
            return idMap.get(id);
        }

        public int getSword() {
            return this.ids[0];
        }

        public int getShovel() {
            return this.ids[1];
        }

        public int getPickaxe() {
            return this.ids[2];
        }

        public int getAxe() {
            return this.ids[3];
        }

        public int getHoe() {
            return this.ids[4];
        }
    }
}
